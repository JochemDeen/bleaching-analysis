function bleachfitter(pfa,width,minamp)
% bleachfitter(pfa,width,minamp)
% pfa = GLRT sensitivity for fitting
% width = width of spot (in pixels) for fitting
% minamp = min amplitude of gaussian (0 = disabled)


%default values, change these if needed
global color; global pixelsize
color=1; %in case of multi color frames
pixelsize=107;

%set the start and end if needed
nextblinkdistance=1;% nextblinkdistance = distance for still same molecule in pixels
SET_start_frame=-1; %Default: last frame
SET_end_frame=-1; %Default: first frame
method=2; %1=basic method, 2=advanced method

%select the filenames that have to be analyzed
[filenames_select,fp] = uigetfile({'*.tif';'*.TIF'},'load bleaching files','multiselect', 'on');
cd(fp)

%Create a cell structure with all the complete filepaths
if ~iscellstr(filenames_select)%in case there is only 1 file
    filenames{1}=filenames_select;
    molecules=1;
else 
    %in case there are multiple files
    molecules=size(filenames_select,2);
    filenames=cell(1,molecules);
    for y=1:molecules
        filenames{y}=char(filenames_select(y));
    end
end


%run for 'molecules' runs
for x=1:molecules
    tic
    
    %Load filename
    CCDname=filenames{x};
    fullpath=[fp CCDname];
    CCDpath=fp;
    
    %This is just a quick check to make sure the file exists and contains data (not an empty file)
    %Load fileinfo 
    fileinfo=dir(fullpath);
    if size(fileinfo,1)>0; filesize=fileinfo.bytes;else filesize=0;end
    if filesize>64
    
    %read the number of frames in the movie
    %[~,framesinmovie]=readCCDim(fullpath, 1,1);
    framesinmovie=length(imfinfo(fullpath));

    %if values not changed in the beginning, set to default values
    %(beginning and end
    if SET_start_frame<=0;st=framesinmovie;else;st=SET_start_frame;end
    if SET_end_frame<=0;en=1;else;en=SET_end_frame;end
    
    %Store the parameters for use in other programs of mine
    flt=store_in_flt(width,pfa,st,en,minamp,nextblinkdistance);
    
    %Create output file
    outfile=filesaving(CCDpath,CCDname);
    
    if method==1;gaussians=bleachingfitting_function(fullpath, flt);
    elseif method==2; gaussians=bleachingfitting_functionPLUS(fullpath, flt);
    end
    
    %saving gaussians (if there are more than 
    if ~isempty(gaussians)
        %Create some initial lists
        gaussiansbackup=gaussians;
        selected=[]; 
        cdir=pwd;
        
        %Save and display the matrices
        cd(CCDpath)
        eval(['save ' outfile '  CCDpath CCDname flt gaussians gaussiansbackup selected'])
        disp(['saved as ' [CCDpath outfile]])
        disp(['found ' num2str(size(gaussians,1)) ' molecules'])
        
        %move back to current directory
        cd(cdir); 
    else
        disp('nothing was fitted')
    end
    else
       disp (['skipped ' filename])
    end
    toc
end


function [previouspositive, previous2positive, positivegaussians]=Positivefit(newim,flt,nextblinkdistance,fr,previouspositive,positivegaussians,queue)
newim=abs(newim-2^16);
[previouspositive, previous2positive,positivegaussians]=gaussianfittingCscript(flt,nextblinkdistance,fr,newim,previouspositive,positivegaussians,queue); %C script


function flt = store_in_flt(width,pfa,st,en,minamp,nextblinkdistance)
 global color
 global pixelsize
flt.psfWidth=width;
flt.pfa=pfa;
flt.stfr=st;
flt.enfr=en;
flt.colourframe=color;
flt.pixelsize=pixelsize;
%flt.resistd=maxres;
flt.nn=10;
flt.minAmp=minamp;
flt.nextblinkdistance=nextblinkdistance;




function outfile=filesaving(filepath,filename)

outfile=filename(1:strfind(filename, '.')-1);

%get date
datum=datevec(now);

%filename is 
outfile=[outfile '_Fits_' num2str(datum(3)) '-'  num2str(datum(2)) '-' num2str(datum(1))];

%remove spaces
outfile=outfile(outfile~=' ');

%remove comma's
outfile2={outfile};
outfile3=strrep(outfile2,',','');
outfile=outfile3{1};
file=[filepath, outfile '.mat'];

%check if the file exists, 
if exist(file)>0
    h=1;
    while h
        
        %give the option to owerwrite the file
        ques=questdlg('the file exist. Overwrite', 'question', 'Yes', 'No', 'No');
        
        %overwrite
        if strcmp(ques, 'Yes')
            delete(file)
            h=0;
        %don't overwrite
        elseif strcmp(ques, 'No')
            
            %give alrternative filepath and name
            prompt ={'Filepath: ', 'Filename: '};
            dlg_title='matlab'; num_line=1; def={filepath, [outfile '.mat']};
            answer=inputdlg(prompt, dlg_title, num_line,def);
            if strcmp(file,[char(answer(1,:)) char(answer(2,:))])>0
                %if the file and whats written down are still the same,
                %continue while loop
                h=1;
            else
                
                %otherwise create new filename
                outfile=char(answer(2));
                filepath=char(answer(1));
                file=[filepath, outfile];
                h=0;
            end
        end
    end
end

