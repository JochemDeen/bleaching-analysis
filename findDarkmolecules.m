function DarkGaussian_Indices=findDarkmolecules(gaussians,dark_gaussians,im,minimal_distance)
%Mask size
mask_size=5;

imsize=[size(im,1),size(im,2)];

%create list with just x and y vectors of the gaussians and dark_gaussians
list1=[gaussians(:,2),gaussians(:,3)];
list2=[dark_gaussians(:,2),dark_gaussians(:,3)];

%create vector with indices of Dark Gaussian
DarkGaussian_Indices=zeros(size(dark_gaussians,1),1);

%loop through all dark gaussians
for i=1:size(list2,1)
    
    %find all locations from the original gaussians vector that are closer than minimal distance to the current dark gaussian
    location=list2(i,:);
    index=distance(list1,location,minimal_distance);
    
    if size(index)>0
        %create new gaussian vector to find out which is off
        trysetgaussians=gaussians(index,:);
        
        %crop the image
        [xmin,xmax]=setminimal(location(1),imsize(2),mask_size);
        [ymin,ymax]=setminimal(location(2),imsize(1),mask_size);
        cropped_im=im(ymin:ymax,xmin:xmax);
        
        %correct Gaussian figures
        trysetgaussians(:,2)=trysetgaussians(:,2)-xmin+1;
        trysetgaussians(:,3)=trysetgaussians(:,3)-ymin+1;
        
        %find the best index for the dark molecule, with lowest residual
        DarkGaussianIndex=findbestfitinimage(trysetgaussians,cropped_im);
        DarkGaussian_Indices(i)=gaussians(DarkGaussianIndex,13);
    end
    
end
DarkGaussian_Indices=DarkGaussian_Indices(DarkGaussian_Indices>0);

function index=distance(list_locations,location,minimal_distance)

distance_vector=sqrt((list_locations(:,1)-location(1)).^2+(list_locations(:,2)-location(2)).^2);

index=find(distance_vector<minimal_distance);

function DarkGaussianIndex=findbestfitinimage(trysetgaussians,im)
%create subtract image (all zeros)
imsize=[size(im,1),size(im,2)];
subtractim=zeros(imsize(1),imsize(2)); %y and x

%use average background value used in fitting
background_av=mean(trysetgaussians(:,7));

av_value=zeros(size(trysetgaussians,1),1);

%loop over all trysetgaussians
for i=1:size(trysetgaussians,1)
    
    %create new gaussians without current gaussian
    index=true(1,size(trysetgaussians,1));
    index(i)=false;
    subtractgaussians=trysetgaussians(index,:);
    
    %subtract gaussians without current from image
    subtractim=create_gaussian_image(subtractgaussians,subtractim);
    newim=im-subtractim;
    
    %calculate residual as value-background squared
    av_value(i)=(mean(mean(newim))-background_av)^2;
end

%the lowest residuel is the molecule that is turned off
[~,DarkGaussianIndex]=min(av_value);
    

function [min,max]=setminimal(location,max_size,mask_size)
min=floor(location-mask_size);
max=min+mask_size;
if min<1;min=1;end
if max>max_size;xmax=max_size;end