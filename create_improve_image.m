function [improved_image,image_database]=create_improve_image(gaussians,im,image_database)
%Mask size
mask_size=5;
improved_image=im;

for i=1:size(gaussians,1)
    xlocation=gaussians(i,2);
    ylocation=gaussians(i,3);
    index=gaussians(i,13);
    
    %the image is an average of fits+1
    image_database(index).fits=image_database(index).fits+1;
    fits=image_database(index).fits;
    
    %cropped image
    [xmin,xmax]=setminimal(xlocation,size(im,2),mask_size);
    [ymin,ymax]=setminimal(ylocation,size(im,1),mask_size);

    cropped_image=im(ymin:ymax,xmin:xmax);
    
    %load current image from database
    curim=image_database(index).image;

    %check if both images still same size, to prevent errors (happens
    %rarely, but still)
    [cropped_image,curim]=checkimage(cropped_image,curim,xmin,xmax,ymin,ymax,size(im,2),size(im,1));
    
    %Average image with old image and current cropped image
    image_database(index).image=(fits-1)*curim./fits+cropped_image./fits;
    
    %replace image with updated form
    improved_image(ymin:ymax,xmin:xmax)=image_database(index).image;
end

function [min,max]=setminimal(location,max_size,mask_size)
min=floor(location-mask_size);
max=min+2*mask_size;
if min<1;min=1;end
if max>max_size;max=max_size;end


function [cropped_image,curim]=checkimage(cropped_image,curim,xmin,xmax,ymin,ymax,maxx,maxy)
if size(curim,1)~=size(cropped_image,1)
    %If sizes are not equal that is because one of the edges goes
    %beyond the limit, solve by making the cur image the same size
    %as the croppedimage (remove lines or add lines)

    %if curim y-direction smaller, add lines
    if size(curim,1)<size(cropped_image,1)
        
        if ymin==1
            %add line on top
            curim(end+1,:)=cropped_image(end,:);
        elseif ymax==maxy
            %add line on bottom
            curim=[cropped_image(1,:);curim];

        end
    end
        
    %if curim y-direction bigger, remove lines
    if size(curim,1)>size(cropped_image,1)
        
        if ymin==1
            %remove line on top
            curim=curim(1:end-1,:);
        elseif ymax==maxy
            %remove line on bottom
            curim=curim(2:end,:);
        end
    end
end
   
if size(curim,2)~=size(cropped_image,2)
    %if curim y-direction smaller, add lines
    if size(curim,2)<size(cropped_image,2)
        
        if xmin==1
            %add line on end
            curim(:,end+1)=cropped_image(:,end);
        elseif xmax==maxx
            %add line on beginning
            curim=[cropped_image(:,1),curim];
        end
    end
        
    %if curim x-direction bigger, remove lines
    if size(curim,2)>size(cropped_image,2)
        
        if ymin==1
            %remove line on end
            curim=curim(:,1:end-1);
        elseif ymax==maxy
            %remove line on beginning
            curim=curim(:,2:end);
        end
    end
        
end