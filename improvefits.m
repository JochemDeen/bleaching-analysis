function gaussians=improvefits(gaussians,improved_gaussians,minimal_distance)

%create list with just x and y vectors of the gaussians and dark_gaussians
list1=[gaussians(:,2),gaussians(:,3)];
list2=[improved_gaussians(:,2),improved_gaussians(:,3)];

%loop through all improved gaussians
for i=1:size(list2,1)

    %find all locations from the original gaussians vector that are closer than minimal distance to the current improved gaussian
    location=list2(i,:);
    
    index=distance(list1,location,minimal_distance);

    
    currentindex=gaussians(index,13);
    if size(currentindex)>=2 %too much molecules, set to permanent state
        gaussians(currentindex,12)=2;
    elseif size(currentindex)==1
        gaussians(currentindex,1:11)=improved_gaussians(i,1:11);        
    end
        
end

function index=distance(list_locations,location,minimal_distance)

distance_vector=sqrt((list_locations(:,1)-location(1)).^2+(list_locations(:,2)-location(2)).^2);

index=find(distance_vector<minimal_distance);
