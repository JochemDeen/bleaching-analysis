function PermGaussian_Indices=findPermmolecules(gaussians,new_gaussians)
%set minimal distance
minimal_distance=5;%~500 nm

%create list with just x and y vectors of the gaussians and dark_gaussians
list1=[gaussians(:,2),gaussians(:,3)];
list2=[new_gaussians(:,2),new_gaussians(:,3)];

PermGaussian_Indices=[];

%loop through all new gaussians
for i=1:size(list2,1)

    %find all locations from the original gaussians vector that are closer than minimal distance to the current dark gaussian
    location=list2(i,:);
    index=distance(list1,location,minimal_distance);
    if size(index)>0
        PermGaussian_Indices=[PermGaussian_Indices;gaussians(index,13)];
    end

end


function index=distance(list_locations,location,minimal_distance)

distance_vector=sqrt((list_locations(:,1)-location(1)).^2+(list_locations(:,2)-location(2)).^2);

index=find(distance_vector<minimal_distance);
