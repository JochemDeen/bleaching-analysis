function PALM_image_maker(pfa,psfWidth,new_width)
if nargin<3
    pfa=25;
    psfWidth=1.6;
    new_width=0.5;
end

%default values, change these if needed
magnify = 4; %increase image pixels by ...
minAmp= 0;

%select the filenames that have to be analyzed
[filenames_select,fp] = uigetfile({'*.tif';'*.TIF'},'load bleaching files','multiselect', 'on');
cd(fp)


%Create a cell structure with all the complete filepaths
if ~iscellstr(filenames_select)%in case there is only 1 file
    filenames{1}=filenames_select;
    molecules=1;
else 
    %in case there are multiple files
    molecules=size(filenames_select,2);
    filenames=cell(1,molecules);
    for y=1:molecules
        filenames{y}=char(filenames_select(y));
    end
end

wb=waitbar(0, ['HR imaging on 0/' num2str(molecules)]);

%run for 'molecules' runs
for x=1:molecules
    tic
    %update waitbar
    waitbar(x/molecules, wb,  ['HR imaging on ' num2str(x) '/' num2str(molecules)]);

    
    %Load filename
    CCDname=filenames{x};
    fullpath=[fp CCDname];
    CCDpath=fp;
    
    %Load the ccd image
    [im,~]=imread(fullpath, 2,1);
    %Load the CCDimage size
    imsize=[size(im,1),size(im,2)];

    
    %This is just a quick check to make sure the file exists and contains data (not an empty file)
    %Load fileinfo 
    fileinfo=dir(fullpath);
    if size(fileinfo,1)>0; filesize=fileinfo.bytes;else filesize=0;end
    if filesize>64
    
    %Localize data
    [Cpos] = LocalizerMatlab('localize', psfWidth,'glrt',pfa, '2DGauss',fullpath);
    
    %convert to our format
    gaussians=converttogaussians(Cpos,minAmp);
    
    %create high resolution image
    high_res_image=zeros(imsize(1)*magnify,imsize(2)*magnify); %y and x
    high_res_image=create_image(gaussians,high_res_image,magnify,new_width);
    
    %save image
    save_image(high_res_image,CCDname);
    toc
    
    end
end
close(wb)

function gaussians=converttogaussians(Cpos,minAmp)
%Cpos= [frame, intensity, width, x, y, background, dev_int, dev_stdev, dev_x, dev_y, dev_back, num_frames]
%         1   ,     2   ,    3 , 4, 5,       6   ,   7    ,     8    ,   9  ,  10  ,    11   ,     12
if ~isempty(Cpos)
    gaussians=[Cpos(:,2),Cpos(:,5),Cpos(:,4),Cpos(:,3),Cpos(:,3),Cpos(:,2)./(Cpos(:,3).*Cpos(:,3).*2*pi),Cpos(:,6),Cpos(:,2),Cpos(:,7),Cpos(:,8),Cpos(:,11)];
    
    gaussians(:,1)=gaussians(:,1)+1;% +1 Localizer starts counting from 0 instead of 1
    gaussians(:,2)=gaussians(:,2)+1;% +1 Localizer starts counting from 0 instead of 1
    
    %check minimal amplitude
    gaussians(gaussians(:,5)<minAmp,:)=[]; %amplitude
    
    %some additional checks to see if the width makes sense
    gaussians(gaussians(:,3)<0.5 | gaussians(:,4)<0.5, :)=[]; %gaussian width
end

function image=create_image(gaussians,image,magnify,new_width)
%This function adds gaussians to an image
%Mask size:
mask_size=10;

%note to self: Matrix(Row, Column)
%out=[frame, x, y, width x, width y, amp, back, total intensity, max of residual, mean of residual, std of residual];
%     1      2  3       4       5    6      7             8            9                   10                11

%set standard data for all locations
amplitude=1000; %use uniform amplitude for all gaussians
widthx=new_width; %use user set width
widthy=new_width; 

if size(gaussians,1)>0
    %wb2=waitbar(0, 'creating image');

    for i=1:1:size(gaussians,1) %Add gaussians found in 2 consecutive frames (within xx distance, see down).
     %   waitbar(i/size(gaussians,1), wb2,  'creating image');

        %Read the location fields
        %multiply with magnify because more pixels in high res image
        xlocation=gaussians(i,2)*magnify; 
        ylocation=gaussians(i,3)*magnify;
        
        %cropped image
        [xmin,xmax]=setminimal(xlocation,size(image,2),mask_size);
        [ymin,ymax]=setminimal(ylocation,size(image,1),mask_size);
        
        %Create a mesh grid
        [x,y]=meshgrid(xmin:xmax,ymin:ymax);
        %[x,y]=meshgrid(1:imsize(2),1:imsize(1)); 
        
        %add the function using the meshgrid to the image
        image(ymin:ymax,xmin:xmax)=image(ymin:ymax,xmin:xmax)+amplitude*exp(-((0.5*(x-xlocation).^2/widthx^2)+(0.5*(y-ylocation).^2/widthy^2)));
    end
    %close(wb2)
end


function [min,max]=setminimal(location,max_size,mask_size)
min=floor(location-mask_size);
max=min+2*mask_size;
if min<1;min=1;end
if max>max_size;max=max_size;end

function save_image(image,filename)
%sofiImage=sofiImage-min(min(sofiImage));
%divl=ceil(max(max(sofiImage))/65535);
%sofiImage=sofiImage./divl;

formatOut = 'yymmdd';
datenow=datestr(now,formatOut);

image=single(image');
new_filename=[filename(1:end-4) '_HR_' datenow '.TIF'];

%imwrite(image,new_filename);

t = Tiff(new_filename,'w');
t.setTag('ImageLength',size(image,1));
t.setTag('ImageWidth',size(image,2));
t.setTag('Photometric',Tiff.Photometric.MinIsBlack);
t.setTag('BitsPerSample',32);
t.setTag('Compression',Tiff.Compression.None);
t.setTag('PlanarConfiguration',Tiff.PlanarConfiguration.Chunky);
t.setTag('SampleFormat',3)
t.write(image);
t.close();
 
 disp(['saved as ' pwd '\' new_filename])

