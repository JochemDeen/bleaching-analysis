function bleachfittertext(st,en,queue,pfa,width,minamp,nextblinkdistance)
% st = Start of fitting (end frame)
% en = End of fitting (first frame)
% queue = 1 for allow queue
% color = for multi colour frames <-- currently set below
% pfa = pfa for fitting
% width = width of spot (in pixels) for fitting
% minamp = min amplitude of gaussian
% nextblinkdistance = distance for still same molecule in pixels
% positivefit = fits gaussians to blinking molecules (turn off = another
% fit) 1 = on 0 = of --> Currently set below
%%%maxres = max residual   --> Currently unused!

color =1;
positivefit=0;
flt.pixelsize=107;
%nextblinkdistance=nextblinkdistancePX/flt.pixelsize; %nm

[fn,fp] = uigetfile('*.txt','Load text file with list of files');

% [filenames_select,fp] = uigetfile({'*.tif';'*.TIF'},'load bleaching files','multiselect', 'on');

cd(fp)
filenames = textread([fp fn], '%s', 'delimiter', '\n');


% if ~iscellstr(filenames_select)
%         filenames=[fp filenames_select];
%  run=1;
% else 
%     run=size(filenames_select,2);
%     for y=1:run
%         filenames(run)=[fp char(filenames_select(run))];
%     end
% end

if ~iscellstr(filenames)
 run=1;
else 
    run=size(filenames,1);
end


 wb=waitbar(0, ['fitting frame 0/' num2str(st-en)]);

for x=1:run
    tic
    if ~iscellstr(filenames_select)
        filename=filenames;
    else 
        filename=filenames{x};
    end
    fileinfo=dir(filename);
    if size(fileinfo,1)>0; filesize=fileinfo.bytes;else filesize=0;end
    if filesize>64
    flt.psfWidth=width;
    flt.pfa=pfa;
    flt.stfr=st;
    flt.colourframe=color;
    %flt.resistd=maxres;
    flt.nn=10;
    flt.minAmp=minamp;
    flt.positivefit=positivefit;
    [~,framesinmovie]=readCCDim(filename, 1,1);
    [im,~]=readCCDim(filename, 2,1);
    imsize=[1, size(im,1), 1, size(im,2)];

    if flt.stfr>framesinmovie;flt.stfr=framesinmovie;st=flt.stfr;end
    flt.enfr=en;
    
    slash=strfind(filename,'\');
    if size(slash,2)==0
        slash=strfind(filename,'/');
    end
    CCDname=filename(slash(end)+1:end);
    CCDpath=filename(1:slash(end));
    outfile=filesaving(filename,flt,positivefit);

    
    gaussians=[];previous=[];previous2=[]; previous2positive=[];previouspositive=[];
    positivegaussians=[];
    substract_im=zeros(imsize(2),imsize(4)); %y and x
    add_im=zeros(imsize(2),imsize(4)); %y and x


    %nowscan=1; %FOR SCAN
    for fr=st:-1:en %le grande loop
        waitbar((st-fr+1)/(st-en+1), wb,  ['fitting frame ' num2str(st-fr+1) '/' num2str(st-en+1)]);

        %FOR SCAN
        %Reset all values to zero when moving during a scan
        %if st-fr==nowscan*data.flt(x).scan  %Reset all values to zero when moving during a scan
        %    nowscan=nowscan+1;
        %    gausdisplay=[];
        %    %gaussians=[]; %ofcourse except gaussians
        %    previous=[];previous2=[]; %Set values to zero;
        %    substract_im=zeros(data.imsize(2),data.imsize(4));        
        %     if subtract_value==1
        %         subtractframe=str2double(get(handles.edit_subtract_frame,'string'));
        %         subtract=readCCDim([data.CCDpath data.CCDname], 2,subtractframe);
        %     else
        %         subtract=zeros(data.imsize(2),data.imsize(4));
        %     end
        %end

        %%%%Read the image
        im=readCCDim(filename, 2,fr); %load original CCD image
        if size(im,3)>1
           im=im(:,:,flt.colourframe); 
        end
        mx=max(im(:));mn=min(im(:));

        %%% image substraction        
        substract_im=subtractionimage(previous2,substract_im,imsize); 
        newim=im-substract_im;
        %%% Add image
        if positivefit==1
            add_im=subtractionimage(previous2positive,add_im,imsize); 
            newim=newim+add_im;
        end

        %%%%%%%Image filtering%&&&%%%%%%%
        %elseif flt.locmethod==2 ONLY 1 supported
        [previous, previous2,gaussians]=gaussianfittingCscript(flt,nextblinkdistance,fr,newim,previous,gaussians,queue);
        %%% Fit reverse
        if positivefit==1
            [previouspositive,previous2positive,positivegaussians]=Positivefit(newim,flt,nextblinkdistance,fr,previouspositive,positivegaussians,queue);
        end


        %%NEW TEMP SAVE
        gaussiansbackup=gaussians;
        selected=[];
        cd(CCDpath)
        if positivefit ==1
            eval('save temp.mat CCDpath CCDname flt gaussians positivegaussians gaussiansbackup selected')
        else
            eval('save temp.mat CCDpath CCDname flt gaussians gaussiansbackup selected')
        end
    end %end le grande for loop
    

    gaussians=vertcat(gaussians,previous); %add anything still left in waiting list

    %saving gaussians
    if ~isempty(gaussians)
        gaussiansbackup=gaussians;
        selected=[]; 
        cdir=pwd;
        cd(CCDpath)
        if positivefit ==1
            eval(['save ' outfile '  CCDpath CCDname flt gaussians positivegaussians gaussiansbackup selected'])
        else
            eval(['save ' outfile '  CCDpath CCDname flt gaussians gaussiansbackup selected'])
        end
        disp(['saved as ' [CCDpath outfile]])
        disp(['found ' num2str(size(gaussians,1)) ' molecules'])
        cd(cdir); 
    else
        disp('nothing was fitted')
    end
    else
       disp (['skipped ' filename])
    end
    toc
end


function [previouspositive, previous2positive, positivegaussians]=Positivefit(newim,flt,nextblinkdistance,fr,previouspositive,positivegaussians,queue)
newim=abs(newim-2^16);
[previouspositive, previous2positive,positivegaussians]=gaussianfittingCscript(flt,nextblinkdistance,fr,newim,previouspositive,positivegaussians,queue); %C script




function [previous, previous2,gaussians]=gaussianfittingCscript(flt,nextblinkdistance,fr,newim,previous,gaussians,queue) %C script
%outfile=[data.CCDpath 'temp.tif'];
psfWidth=flt.psfWidth;
pfa=flt.pfa;

%out=[x, y, width x, width y, amp, back, total intensity, max of residual, mean of residual, std of residual];
%     1  2    3        4       5    6          7                8               9                   10
%pause(1)

%Note it actually reverse!!!!
%Why...
%     - Ellipsoidal2DGauss:
%     0. frame number
%     1. integrated intensity
%     2. standard deviation in x direction
%     3. standard deviation in y direction
%     4. x position
%     5. y position
%     6. correlation between x and y
%     7. background level (offset)
%     8. deviation on the integrated intensity
%     9. deviation on the standard deviation in x direction
%     10. deviation on the standard deviation in y direction
%     11. deviation on the x position
%     12. deviation on the y position
%     13. deviation on the correlation
%     14. deviation on the background
%     15. number of frames in which the emitter is assumed to be present

Cpos = LocalizerMatlab('localize', psfWidth, 'glrt', pfa, '2DGauss',newim);
if ~isempty(Cpos)
    out=[Cpos(:,5),Cpos(:,4),Cpos(:,3),Cpos(:,3),Cpos(:,2)./(Cpos(:,3).*Cpos(:,3).*2*pi),Cpos(:,6),Cpos(:,2),Cpos(:,7),Cpos(:,8),Cpos(:,8)];
    %out=[Cpos(:,6),Cpos(:,5),Cpos(:,4),Cpos(:,3),Cpos(:,2)./(Cpos(:,3).*Cpos(:,4).*2*pi),Cpos(:,8),Cpos(:,2),Cpos(:,9),Cpos(:,11),Cpos(:,11)];
    out(:,1)=out(:,1)+1;% +1 bc fitting starts from 0 instead of 1
    out(:,2)=out(:,2)+1;% +1 bc fitting starts from 0 instead of 1
    out=[out fr*ones(size(out,1),1)];
    out(out(:,5)<flt.minAmp,:)=[]; %amplitude
    out(out(:,3)>flt.nn | out(:,4)>flt.nn, :)=[]; %gaussina width
    out(out(:,3)<0.5 | out(:,4)<0.5, :)=[]; %gaussian width
    %out(out(:,10)>flt.resistd,:)=[]; %st dev residual
    out=cat(2, ones(size(out,1),1).*fr, out); %add frame number
end


if ~isempty(Cpos)
    if fr~=flt.enfr
        [previous2,previous,gaussians]=queuing(previous,out,nextblinkdistance,gaussians,queue);
    else
        previous2=out; %New gaussians used for subtraction 
        gaussians=vertcat(gaussians,previous2); 
    end
    %disp(['frame = ' num2str(fr) ' ::: ' num2str(size(out,1)) ' molecules and ' num2str(size(previous2,1)) ' gaussians from ' num2str(fr+1)])
else
    %disp(['frame = ' num2str(fr) ' ::: 0 molecules']);
    previous=[];previous2=[]; %Because nothing this time
end


function outfile=filesaving(filename,flt,positivefit)
slash=strfind(filename,'\');
if size(slash,2)==0
    slash=strfind(filename,'/');
end
outfile=filename(slash(end)+1:findstr(filename, '.')-1);
datum=datevec(now);

outfile=['FIT-' outfile];
outfile=[outfile '-fr_' num2str(flt.stfr) '-' num2str(flt.enfr) '_' num2str(datum(3)) '-'  num2str(datum(2)) '-' num2str(datum(1))];
outfile=outfile(outfile~=' ');
outfile2={outfile};
outfile3=strrep(outfile2,',','');
outfile=outfile3{1};
file=[filename(1:slash(end)-1), outfile '.mat'];
if exist(file)>0
    h=1;
    while h
        ques=questdlg('the file exist. Overwrite', 'question', 'Yes', 'No', 'No');
        if strcmp(ques, 'Yes')
            delete(file)
            h=0;
        elseif strcmp(ques, 'No')
            prompt ={'Filepath: ', 'Filename: '};
            dlg_title='matlab'; num_line=1; def={data.CCDpath, [outfile '.mat']};
            answer=inputdlg(prompt, dlg_title, num_line,def);
            if strcmp(file,[char(answer(1,:)) char(answer(2,:))])>0
                h=1;
            else
                outfile=char(answer(2));
                Data.CCDpath=char(answer(1));
                file=[data.CCDpath, outfile];
                h=0;
            end
        end
    end
end

function [previous2,previous,gaussians]=queuing(previous,out,nextblinkdistance,gaussians,queue)% Queu for multiple molecules
queuevalue=queue;
if queuevalue==1 %Waitinglist    

    %Compare OUT (current fit) to Previous (previous fit) "waitinglist"
    previous2=[]; %This value will contain molecules that are found in this frame and in the previous frame, containing the highest intensity.
    mindistance=[]; %This value will contain minimal value
    if ~isempty(previous) && ~isempty(out)
        distance=sqrt((out(:,2)*(ones(size(previous(:,2)))')-(ones(size(out(:,2)))*previous(:,2)')).^2+(out(:,3)*(ones(size(previous(:,3)))')-(ones(size(out(:,3)))*previous(:,3)')).^2); 
        %this creates matrix where the rows indicate the index of the current fit and the columns indicate the index of the previous file
        [mind,I]=min(distance'); %minimal distance in row & index (column number=index in previous file);
        if size(distance,2)==1; I=1;end
        mindistance=[mind;I]'; %2-column matrix minimal distance and index
        %maybe something to get doubles out?....   
        outindex=[1:1:(size(mindistance,1))]';
        mindistance=[outindex, mindistance,(mindistance(:,1)<nextblinkdistance)]; %Index of out file, minimal distance to point x, index of point x in previous, is it close enough
        mindistance1=mindistance(mindistance(:,4)==1,:); %note: maybe 0.5 too low, is only ~24nm... Note is always 1 pixel (upgrade from 0.5 pixel) , maybe dependant objective?
        tempfileprevious=out(mindistance(mindistance(:,4)==0),:);
        tempfileprevious2a=out(mindistance1((out(mindistance1(:,1),6)>1.1*previous(mindistance1(:,3),6)),1),:); %if out-amplitude is larger than 1.1*previous-amplitude
        tempfileprevious2b=previous(mindistance1((out(mindistance1(:,1),6)<=1.1*previous(mindistance1(:,3),6)),3),:);%if out-amplitude is smaller than 1.1*previous-ampltiude
        previous=tempfileprevious;
        previous2=[tempfileprevious2a;tempfileprevious2b];
        gaussians=vertcat(gaussians,previous2);%This is every gaussian together, not used for subtraction
    elseif isempty(previous); previous=out; out=[];
    elseif isempty(out);previous=[];
    end

else %No waiting list
    previous2=out; %New gaussians used for subtraction 
    gaussians=vertcat(gaussians,previous2);%This is every gaussian together, not used for subtraction
end  

function substract_im=subtractionimage(previous2,substract_im,imsize)

%%%%%%%%%newimage=im-Subtraction%%%%%%%%%
%note to self: Matrix(Row, Column)
%out=[frame, x, y, width x, width y, amp, back, total intensity, max of residual, mean of residual, std of residual];
%     1      2  3       4       5    6      7             8            9                   10                11

%Method 1/2 = gaussian subtraction only supported
if size(previous2,1)>0
    for G=1:1:size(previous2,1) %Add gaussians found in 2 consecutive frames (within xx distance, see down).
        amplitude=previous2(G,6);
        widthx=previous2(G,4);
        widthy=previous2(G,5);
        xlocation=previous2(G,2);
        ylocation=previous2(G,3);
        [x,y]=meshgrid(1:imsize(4),1:imsize(2)); %switch 2 and 4??
        substract_im=substract_im+amplitude*exp(-((0.5*(x-xlocation).^2/widthx^2)+(0.5*(y-ylocation).^2/widthy^2)));
    end
end
