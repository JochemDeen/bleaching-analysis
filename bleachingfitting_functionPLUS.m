function gaussians=bleachingfitting_functionPLUS(filename, flt)
%Runs a bleaching analysis on filename using the specifications in flt
%flt contains:
%flt.enfr               last frame (default 1)
%flt.stfr               first frame (default last frame in movie)
%flt.width              width of PSF
%flt.pfa                GLRT sensitivity
%flt.nextblinkdistance  max distance for waitinglist

%Load the ccd image
[im,~]=imread(filename, 2,1);
%Load the CCDimage size
imsize=[size(im,1),size(im,2)];

%initializiation
gaussians=[];waitinglist=[];newly_approved_gaussians=[];
new_gaussians=[];bright_gaussians=[];image_database=[];
Perm_gaussians=[];on_gaussians=[];
subtract_im=zeros(imsize(1),imsize(2)); %y and x
st=flt.stfr;en=flt.enfr;

image=readfullimage(filename,en,st);

info=imfinfo(filename);
BitDepth=info.BitDepth;

%create a waitbar for the bleaching analysis
wb=waitbar(0, ['fitting frame 0/' num2str(st-en)]);

%walk back
for fr=st:-1:en
    %update waitbar
    waitbar((st-fr+1)/(st-en+1), wb,  ['fitting frame ' num2str(st-fr+1) '/' num2str(st-en+1)]);

    %select image
    im=image(:,:,fr);
    im=double(im);
    
    %create bright_gaussians
    if size(gaussians,1)>0
        bright_gaussians=gaussians(gaussians(:,12)>=1,:);
    end
    
    %create subtraction image using all bright gaussians
    subtract_im_All_gaussians=zeros(imsize(1),imsize(2)); %y and x
    subtract_im_All_gaussians=create_gaussian_image(bright_gaussians,subtract_im_All_gaussians); 
   
    %subtract all bright gaussians
    new_im=im-subtract_im_All_gaussians;
    invert_im=invertimage(new_im,BitDepth);
    
    %check for 'dark spots'
    dark_gaussians=Gaussian_Localizer(flt,fr,invert_im);
    if size(dark_gaussians,1)>0 && size(gaussians,1)>0
        %Find the indices that create the lowest residual
        %use only currently bright molecules when deciding if a current molecule is currently dark
        DarkGaussian_Indices=findDarkmolecules(bright_gaussians,dark_gaussians,im,flt.nextblinkdistance);
        
        %set gaussians with index DarkGaussians_Indices to 0 (molecule is off)
        gaussians(DarkGaussian_Indices,12)=0;
    end
        
    %Check for new Gaussians
    new_gaussians=Gaussian_Localizer(flt,fr,new_im);
    
    %Check if these molecules overlap with previously fitted molecules
    if size(new_gaussians,1)>0 && size(gaussians,1)>0
        %check what molecules need to be turned 'PERM'
        PermGaussian_Indices=findPermmolecules(gaussians,new_gaussians);
        gaussians(PermGaussian_Indices,12)=2;
    end
    
    %Check if the new gaussians are in the waitinglist
    if fr~=flt.enfr
        [newly_approved_gaussians,waitinglist]=waitlist(waitinglist,new_gaussians,flt);
        [gaussians,image_database]=Add_all_new_gaussians(gaussians,newly_approved_gaussians,image_database,new_im);  
    else
        %If we reach the last frame, we add all the new fits to the gaussian list (no waitinglist)

        %Note if there is no waitinglist, this is also the code for skipping that waitinglist
        if ~isempty(new_gaussians)
            newly_approved_gaussians=new_gaussians;
            [gaussians,image_database]=Add_all_new_gaussians(gaussians,newly_approved_gaussians,image_database,new_im);
        end
    end
    
    %create perm_gaussians
    if size(gaussians,1)>0
        on_gaussians=gaussians(gaussians(:,12)==1,:);
        Perm_gaussians=gaussians(gaussians(:,12)==2,:);
    end

    %%% create subtraction image from permanent gaussians
    subtract_im=zeros(imsize(1),imsize(2)); %y and x
    if size(Perm_gaussians,1)>0
        subtract_im=create_gaussian_image(Perm_gaussians,subtract_im); 
    end
    
    %create starting image for improved image fitting
    Perm_im=im-subtract_im;

    %improve the image by adding the image database
    if size(on_gaussians,1)>0
        [improved_image,image_database]=create_improve_image(on_gaussians,Perm_im,image_database);
        
        %fit the improved_image
        improved_gaussians=Gaussian_Localizer(flt,fr,improved_image);
        
        %use the improved_gaussians fitted with the improved_image to improve the older gaussians (replace them by the new fit)
        if size(improved_gaussians,1)>0
            gaussians=improvefits(gaussians,improved_gaussians,flt.nextblinkdistance);
        end
    end
end
close(wb);


%gaussians=vertcat(gaussians,waitinglist); %add anything still left in waiting list

function image=invertimage(image,BitDepth)
image=abs(image-2^BitDepth);
image=image-min(min(image));

function image=readfullimage(filename,st,en)

%create waitbar for loading file
wb1=waitbar(0, ['loading file 0/' num2str(en-st)]);

%Note, currently does not take into account multiple color images,
%otherwise will load 3d image rather than 2d
for i=st:1:en
    image(:,:,i)=imread(filename, 2,i);
    waitbar((i-st+1)/(en-st+1), wb1,  ['loading frame ' num2str(i) '/' num2str(en)]);
end
close(wb1);

function gaussians=Gaussian_Localizer(flt,fr,newim) %C script
psfWidth=flt.psfWidth;
pfa=flt.pfa;
gaussians=[];
%gaussians=[frame, x, y, width x, width y, amp, back, total intensity, dev intensity, dev stdev, dev back];
%              1   2 3      4       5        6    7           8              9        10           11

%Gaussian fitting
Cpos = LocalizerMatlab('localize', psfWidth, 'glrt', pfa, '2DGauss',newim);


%Cpos= [frame, intensity, width, x, y, background, dev_int, dev_stdev, dev_x, dev_y, dev_back, num_frames]
%           1   ,     2    ,    3 , 4, 5,       6   ,   7   ,     8     ,   9  ,  10  ,    11   ,     12
if ~isempty(Cpos)
    gaussians=[Cpos(:,5),Cpos(:,4),Cpos(:,3),Cpos(:,3),Cpos(:,2)./(Cpos(:,3).*Cpos(:,3).*2*pi),Cpos(:,6),Cpos(:,2),Cpos(:,7),Cpos(:,8),Cpos(:,11)];
    
    gaussians(:,1)=gaussians(:,1)+1;% +1 Localizer starts counting from 0 instead of 1
    gaussians(:,2)=gaussians(:,2)+1;% +1 Localizer starts counting from 0 instead of 1
    
    %check minimal amplitude
    gaussians(gaussians(:,5)<flt.minAmp,:)=[]; %amplitude
    
    %some additional checks to see if the width makes sense
    gaussians(gaussians(:,3)>flt.nn | gaussians(:,4)>flt.nn, :)=[]; %gaussina width
    gaussians(gaussians(:,3)<0.5 | gaussians(:,4)<0.5, :)=[]; %gaussian width
    
    %out(out(:,10)>flt.resistd,:)=[]; %st dev residual
    
    %add the current frame number
    gaussians=cat(2, ones(size(gaussians,1),1).*fr, gaussians); %add frame number
    
    %Add a final column (12) which indicates if molecule is on (1) or off (0)
    gaussians=[gaussians,ones(size(gaussians,1),1)];

end

function [gaussians,image_database]=Add_all_new_gaussians(gaussians,newly_approved_gaussians,image_database,new_im)
%check the last index of the current gaussian list
if size(gaussians,1)>0;last_index=gaussians(end,13);else last_index=0;end;

%add a column (13) that indicates the index of the gaussian in this matrix
index_newly_approved_gaussians=[1:size(newly_approved_gaussians,1)]+last_index;
newly_approved_gaussians=[newly_approved_gaussians,index_newly_approved_gaussians'];

%put image in database
if size(new_im,1)>0
    image_database=add_to_image_database(newly_approved_gaussians,new_im,image_database);
end

gaussians=vertcat(gaussians,newly_approved_gaussians); 


