function image_database=add_to_image_database(gaussians,im,image_database)
%Mask size
mask_size=5;

for i=1:size(gaussians)
    xlocation=gaussians(i,2);
    ylocation=gaussians(i,3);
    index=gaussians(i,13);
    
    %cropped image
    [xmin,xmax]=setminimal(xlocation,size(im,2),mask_size);
    [ymin,ymax]=setminimal(ylocation,size(im,1),mask_size);

    cropped_image=im(ymin:ymax,xmin:xmax);
    image_database(index).image=cropped_image;
    image_database(index).fits=1;

end


function [min,max]=setminimal(location,max_size,mask_size)
min=floor(location-mask_size);
max=min+2*mask_size;
if min<1;min=1;end
if max>max_size;max=max_size;end