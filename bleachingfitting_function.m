function gaussians=bleachingfitting_function(filename, flt)
%Runs a bleaching analysis on filename using the specifications in flt
%flt contains:
%flt.enfr               last frame (default 1)
%flt.stfr               first frame (default last frame in movie)
%flt.width              width of PSF
%flt.pfa                GLRT sensitivity
%flt.nextblinkdistance  max distance for waitinglist

%Load the ccd image
[im,~]=imread(filename, 2,1);
%Load the CCDimage size
imsize=[size(im,1),size(im,2)];

%initializiation
gaussians=[];waitinglist=[];newly_approved_gaussians=[];new_gaussians=[];
substract_im=zeros(imsize(1),imsize(2)); %y and x
st=flt.stfr;en=flt.enfr;

image=readfullimage(filename,en,st);

%create a waitbar for the bleaching analysis
wb=waitbar(0, ['fitting frame 0/' num2str(st-en)]);

%walk back
for fr=st:-1:en
    %update waitbar
    waitbar((st-fr+1)/(st-en+1), wb,  ['fitting frame ' num2str(st-fr+1) '/' num2str(st-en+1)]);

    %select image
    im=image(:,:,fr);
    
    im=double(im);
    %%% image substraction        
    substract_im=create_gaussian_image(newly_approved_gaussians,substract_im); 
    newim=im-substract_im;

    %Gaussianfitting
    new_gaussians=Gaussian_Localizer(flt,fr,newim);
    
    %Check if the new gaussians are in the waitinglist
    if fr~=flt.enfr
        [newly_approved_gaussians,waitinglist]=waitlist(waitinglist,new_gaussians,flt);
        gaussians=vertcat(gaussians,newly_approved_gaussians); 
    else
        %If we reach the last frame, we add all the new fits to the gaussian list (no waitinglist)

        %Note if there is no waitinglist, this is also the code for skipping that waitinglist
        if ~isempty(new_gaussians)
            newly_approved_gaussians=new_gaussians; 
            gaussians=vertcat(gaussians,newly_approved_gaussians); 
        end
    end


end
close(wb);


gaussians=vertcat(gaussians,waitinglist); %add anything still left in waiting list

function image=readfullimage(filename,st,en)

%create waitbar for loading file
wb1=waitbar(0, ['loading file 0/' num2str(en-st)]);

%Note, currently does not take into account multiple color images,
%otherwise will load 3d image rather than 2d
for i=st:1:en
    image(:,:,i)=imread(filename, 2,i);
    waitbar((i-st+1)/(en-st+1), wb1,  ['loading frame ' num2str(i) '/' num2str(en)]);
end
close(wb1);

function gaussians=Gaussian_Localizer(flt,fr,newim) %C script
psfWidth=flt.psfWidth;
pfa=flt.pfa;
gaussians=[];
%gaussians=[frame, x, y, width x, width y, amp, back, total intensity, dev intensity, dev stdev, dev back];
%              1   2 3      4       5        6    7           8              9        10           11

%Gaussian fitting
Cpos = LocalizerMatlab('localize', psfWidth, 'glrt', pfa, '2DGauss',newim);


%Cpos= [frame, intensity, width, x, y, background, dev_int, dev_stdev, dev_x, dev_y, dev_back, num_frames]
%           1   ,     2    ,    3 , 4, 5,       6   ,   7   ,     8     ,   9  ,  10  ,    11   ,     12
if ~isempty(Cpos)
    gaussians=[Cpos(:,5),Cpos(:,4),Cpos(:,3),Cpos(:,3),Cpos(:,2)./(Cpos(:,3).*Cpos(:,3).*2*pi),Cpos(:,6),Cpos(:,2),Cpos(:,7),Cpos(:,8),Cpos(:,11)];
    
    gaussians(:,1)=gaussians(:,1)+1;% +1 Localizer starts counting from 0 instead of 1
    gaussians(:,2)=gaussians(:,2)+1;% +1 Localizer starts counting from 0 instead of 1
    
    %check minimal amplitude
    gaussians(gaussians(:,5)<flt.minAmp,:)=[]; %amplitude
    
    %some additional checks to see if the width makes sense
    gaussians(gaussians(:,3)>flt.nn | gaussians(:,4)>flt.nn, :)=[]; %gaussina width
    gaussians(gaussians(:,3)<0.5 | gaussians(:,4)<0.5, :)=[]; %gaussian width
    
    %out(out(:,10)>flt.resistd,:)=[]; %st dev residual
    
    %add the current frame number
    gaussians=cat(2, ones(size(gaussians,1),1).*fr, gaussians); %add frame number
end


