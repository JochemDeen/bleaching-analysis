function [newly_approved_gaussians,waitinglist]=waitlist(waitinglist,new_gaussians,flt)
%This function compares new_gaussians (current fit) to
%waitinglist(waitinglist fit) and delivers a list of approved gaussians

%initialize waitlist
newly_approved_gaussians=[]; %This value will contain molecules that are found in this frame and in the waitinglist frame, containing the highest intensity.

%set distance for waitlist
nextblinkdistance=flt.nextblinkdistance;

if ~isempty(waitinglist) && ~isempty(new_gaussians)
    
    %calculate all the distances between the new_gaussian and the waitinglist locations
    distance=sqrt((new_gaussians(:,2)*(ones(size(waitinglist(:,2)))')-(ones(size(new_gaussians(:,2)))*waitinglist(:,2)')).^2+(new_gaussians(:,3)*(ones(size(waitinglist(:,3)))')-(ones(size(new_gaussians(:,3)))*waitinglist(:,3)')).^2); 
    
    %this creates matrix where the rows indicate the index of the current fit and the columns indicate the index of the waitinglist file
    [mind,I]=min(distance'); %minimal distance in row & index (column number=index in waitinglist file);
    
    %in case of only 1 distance, there is only 1 index
    if size(distance,2)==1; I=1;end
    
    %This function creates a 2-column matrix with minimal distance and index of the minimal distance
    mindistance=[mind;I]'; 
    
    %create an index list for the new gaussian list
    new_gaussiansindex=[1:1:(size(mindistance,1))]';
    
    %create a new (4 column) matrix with index of the new_gaussians(1), the minimal distance to a location in the waitinglist (2),  the index of that location in the waitinglist (3) and if that distance is less than the maximal distance (4)
    mindistance=[new_gaussiansindex, mindistance,(mindistance(:,1)<nextblinkdistance)]; 
    
    %create a new matrix with only the molecules that are closer than nextblinkdistance to each other
    mindistance1=mindistance(mindistance(:,4)==1,:);
    
    %tempfilewaitinglist2a=new_gaussians(mindistance1((new_gaussians(mindistance1(:,1),6)>1.1*waitinglist(mindistance1(:,3),6)),1),:); %if new_gaussians-amplitude is larger than 1.1*waitinglist-amplitude
    %tempfilewaitinglist2b=waitinglist(mindistance1((new_gaussians(mindistance1(:,1),6)<=1.1*waitinglist(mindistance1(:,3),6)),3),:);%if new_gaussians-amplitude is smaller than 1.1*waitinglist-ampltiude
    %newly_approved_gaussians=[tempfilewaitinglist2a;tempfilewaitinglist2b];
    
    %the newly approved gaussians are the new gaussians, whose distance is closest to the waitinglists
    newly_approved_gaussians=new_gaussians(mindistance1(:,1),:);
    
    
    
    %The molecules not close enough to other molecules will be make up the new waitinglist
    waitinglist=new_gaussians(mindistance(mindistance(:,4)==0),:);

elseif isempty(waitinglist); 
    
    %if there was nothing in the waitinglist, the new waitinglist is all of the new gaussians
    waitinglist=new_gaussians; 
    new_gaussians=[];
elseif isempty(new_gaussians);
    %if there were no new fits, the new waitinglist is emptied
    
    waitinglist=[];
end
